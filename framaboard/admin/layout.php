<!DOCTYPE html>
<html>
    <head>
        <title><?php echo TITLE; ?> (administration)</title>
        <meta charset="UTF-8" />
        <meta name="viewport" content="initial-scale=1.0" />
        <script src="https://framasoft.org/nav/lib/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="https://framasoft.org/nav/lib/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <link href="../css/default.css" media="all" rel="stylesheet">
        <link href="../css/frama.board.css" media="all" rel="stylesheet">
    </head>
    <body class="fb_g2">
        <script src="https://framasoft.org/nav/nav.js" type="text/javascript"></script>
        <div class="container ombre">
            <?php
                $title = TITLE;
                if (starts_with($title, 'Frama')) {
                    $title = '<span class="frama">Frama</span>'
                           . '<span class="vert">' . substr($title, 5) . '</span>';
                }
            ?>
            <p class="text-right"><a href="//<?php echo URL_BASE; ?>">
                <i class="glyphicon glyphicon-home"></i>
                Retour à l’accueil
            </a></p>

            <h1><a href="//<?php echo URL_BASE; ?>/admin"><?php echo $title; ?> (administration)</a></h1>

            <hr/>

            <?php if (!empty($flash)) { ?>
            <div class="row">
                <div class="col-md-12">
                    <?php foreach ($flash as $status => $message) { ?>
                        <div class="alert alert-<?php echo $status; ?> alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <?php echo $message; ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <?php } ?>

            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <form method="GET" action="">
                        <div class="form-group">
                            <label for="search-account">Rechercher un compte</label>
                            <input type="text" name="search" id="search-account" class="form-control" <?php echo isset($_GET['search']) ? 'value="' . $_GET['search'] . '"' : ''; ?> />
                        </div>
                        <input type="hidden" name="do" value="search" />
                        <input type="submit" class="btn btn-primary btn-block" value="Rechercher" />
                    </form>
                </div>
                <div class="col-md-4"></div>
            </div>

            <?php if (isset($users)) { ?>
            <div class="row">
                <div class="col-md-12">
                    <h2>Utilisateurs du compte <?php echo $_GET['search']; ?></h2>
                    <ul>
                        <?php foreach ($users as $user) { ?>
                            <li>
                                <?php echo $user['username']; ?>
                                <?php echo $user['is_admin'] == 1 ? '(administrateur)' : ''; ?>
                                — <a href="mailto:<?php echo $user['email']; ?>"><?php echo $user['email']; ?></a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <?php } ?>

            <?php $nb_accounts = $paginator->count(); ?>
            <h2>Liste des comptes (<?php echo $nb_accounts; ?> compte<?php echo $nb_accounts > 1 ? 's' : ''; ?>)</h2>

            <?php if (isset($error)) { ?>
                <div class="alert alert-danger"><strong>Mince&nbsp;!</strong> <?php echo $error; ?></div>
            <?php } ?>

            <?php include('pagination.php'); ?>

            <?php $items = $paginator->items(); ?>
            <?php if (count($items) > 0) { ?>
            <ul class="list-group">
                <?php foreach($items as $item) { ?>
                    <li class="list-group-item">
                        <h4 class="list-group-item-heading"><?php echo $item['name']; ?></h4>
                        <p class="list-group-item-text">
                            <a class="btn btn-default" href="?do=see&amp;search=<?php echo $item['name']; ?>">Voir les utilisateurs</a>
                            <a class="btn btn-default" href="//<?php echo $item['name']; ?>.<?php echo URL_BASE; ?>">Accéder au compte</a>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#passwordModal" data-account="<?php echo $item['name']; ?>">Réinitialiser un mot de passe</button>
                            <a class="btn btn-danger" data-confirm="true" href="?do=delete&amp;account=<?php echo $item['name']; ?>&amp;token=<?php echo $token; ?>">Supprimer ce compte</a>
                        </p>
                    </li>
                <?php } ?>
            </ul>
            <?php } else { ?>
            <div class="alert alert-info">Aucun compte à afficher.</div>
            <?php } ?>

            <?php include('pagination.php'); ?>

            <!-- Modal -->
            <div class="modal fade" id="passwordModal" tabindex="-1" role="dialog" aria-labelledby="passwordModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="passwordModalLabel">
                                Réinitialiser un mot de passe du compte <span id="passwordAccountName">#</span>
                            </h4>
                        </div>
                        <form method="POST" action="?do=reset&amp;token=<?php echo $token; ?>">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="username">Nom utilisateur</label>
                                    <input type="text" class="form-control" name="username" id="username" />
                                </div>
                                <div class="form-group">
                                    <label for="newPassword">Nouveau mot de passe (en clair)</label>
                                    <div class="input-group">
                                      <input type="text" class="form-control" name="newPassword" id="newPassword" />
                                      <span class="input-group-addon">
                                          <a href="#" id="passwordGenerator"><i class="glyphicon glyphicon-refresh"></i></a>
                                      </span>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" name="account" id="account" value="" />
                                <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                                <input type="submit" class="btn btn-primary" value="Changer le mot de passe" />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

<script>
function generatePassword() {
    var length = 8,
        charset = "abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
}

$(document).ready(function () {
    $('[data-confirm="true"]').click(function (evt) {
        return confirm('Êtes-vous certain de vouloir continuer (action irréversible) ?');
    });

    $('[data-toggle="modal"]').click(function (evt) {
        var account_name = $(this).data('account');
        $('#passwordAccountName').html(account_name);
        $('#account').val(account_name);
    });

    $('#passwordGenerator').click(function (evt) {
        $('#newPassword').val(generatePassword());
        return false;
    });
});
</script>
    </body>
</html>
